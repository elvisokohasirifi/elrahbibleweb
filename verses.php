<?php 
	header('Access-Control-Allow-Origin: *');
	session_start();

	class MyDB extends SQLite3 {
      function __construct() {
         $this->open('bible-sqlite.db');
      }
   	}

   	function loadBooks(){
		$db = new MyDB();
		if(!$db) {
		  echo $db->lastErrorMsg();
		} 
	    $ret = $db->query("select * from key_english");
		$ans = '';
	    $count = 0;
		while($row = $ret->fetchArray(SQLITE3_ASSOC) ) { 
	      if($count == $_SESSION['chapters'])  
	        $ans = '<li class="current"><a href="functions.php?book='.$row['b'].'" title="'.$row['n'].'">'.$row['n'].'</a></li>';
	      else         
	        $ans .= '<li><a href="functions.php?book='.$row['b'].'" title="'.$row['n'].'">'.$row['n'].'</a></li>';
	      $count++;
	    }
	    $db->close();
	    return $ans;
	}


	function loadVerses(){
	    $db = new MyDB();
	    if(!$db) {
	      echo $db->lastErrorMsg();
	    } 
	    $ret = $db->query("select * from t_asv where b =" . $_SESSION['chapters'] . " and c =" . $_SESSION['verse']);
	    $ans = '<div class="btn-group">';
	    while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {            
	      $ans .= '<p style="margin:10px; color:white" onclick="location.href=\'functions.php?verse='.$row['v'].'\'"><span style="font-size:9px; color: #3e8e41">'.$row['v'].' </span> '.$row['t'].'</p>';
	    }
	    $ans .= '</div>';
	    $db->close();
	    return $ans;
	}

	if (isset($_GET['selectv'])) {
	    echo loadVerses();
	}

	if (isset($_GET['select'])) {
		echo loadBooks();
	}

 ?>
