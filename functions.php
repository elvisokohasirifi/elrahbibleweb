<?php 
  header('Access-Control-Allow-Origin: *');
  session_start();
	class MyDB extends SQLite3 {
      function __construct() {
         $this->open('bible-sqlite.db');
      }
   	}

	function loadBooks(){
		$db = new MyDB();
		if(!$db) {
		  echo $db->lastErrorMsg();
		} 
    $ret = $db->query("select * from key_english");
		$ans = '';
    $count = 0;
		while($row = $ret->fetchArray(SQLITE3_ASSOC) ) { 
      if($count == $_SESSION['chapters'])  
        $ans = '<li class="current"><a href="functions.php?book='.$row['b'].'" title="'.$row['n'].'">'.$row['n'].'</a></li>';
      else         
        $ans .= '<li><a href="functions.php?book='.$row['b'].'" title="'.$row['n'].'">'.$row['n'].'</a></li>';
      // <li class="current">
    }
    $db->close();
    return $ans;
	}


  function loadBook(){
    $db = new MyDB();
    if(!$db) {
      echo $db->lastErrorMsg();
    } 
    $ret = $db->query("select * from key_english");
    $ans = '<div class="btn-group">';
    while($row = $ret->fetchArray(SQLITE3_ASSOC) ) {            
      $ans .= '<button class="h6" style="margin:0px" onclick="location.href=\'functions.php?book='.$row['b'].'\'">'.$row['n'].'</button>';
    }
    $ans .= '</div>';
    $db->close();
    return $ans;
  }

  function loadrandomverse(){
    $db = new MyDB();
    if(!$db) {
      echo $db->lastErrorMsg();
    } 
    $ret = $db->query("select * from t_asv where b = 20 order by RANDOM() limit 1");
    $row = $ret->fetchArray(SQLITE3_ASSOC);
    $db->close();
    return $row['t'];
  }

	if (isset($_GET['select'])) {
		echo loadBooks();
	}

  if (isset($_GET['selects'])) {
    echo loadBook();
  }

  if (isset($_GET['book'])) {
    $_SESSION['chapters'] = $_GET['book'];
    header("Location: chapters.html");
  }

  if (isset($_GET['chapter'])) {
    $_SESSION['verse'] = $_GET['chapter'];
    header("Location: verses.html");
  }

  if (isset($_GET['selected'])) {
    echo loadrandomverse();
  }
 ?>