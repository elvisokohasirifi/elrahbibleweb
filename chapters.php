<?php 
	header('Access-Control-Allow-Origin: *');
	#include 'functions.php';
	session_start();

	class MyDB extends SQLite3 {
      function __construct() {
         $this->open('bible-sqlite.db');
      }
   	}


	function loadChapters(){
	    global $chapters;
	    $db = new MyDB();
	    if(!$db) {
	      echo $db->lastErrorMsg();
	    } 
	    $ret = $db->query("select max(c) as chapts from t_asv where b = ". $_SESSION['chapters']);
	    $row = $ret->fetchArray(SQLITE3_ASSOC);
	    $ans = '<div class="btn-group">';   
	    for($i = 1; $i <= $row['chapts']; $i++) {        
	      $ans .= '<button class="h6" style="margin-top:20px; text-align: center; font-size:20px;" onclick="location.href=\'functions.php?chapter='.$i.'\'"><span style="font-size: 6px;">Chapter<br></span>'.$i.'</button>';
	    }
	    $ans .= '</div>';
	    $db->close();
	    return $ans;
	}


	if (isset($_GET['selectc'])) {
	    echo loadChapters();
	}

 ?>